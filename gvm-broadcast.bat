@echo off

:gvmtool_broadcast
	if not "!BROADCAST_HITS!"=="" (
		echo !BROADCAST_HITS!
	) else (
		echo !BROADCAST_LIVE!
		echo.
	)
exit /b 0