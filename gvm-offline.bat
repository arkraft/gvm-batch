@echo off

:gvmtool_offline
	if "%~1"=="enable" (
		set "GVM_FORCE_OFFLINE=true"
		echo Forced offline mode enabled.
	)
	if "%~1"=="disable" (
		set "GVM_FORCE_OFFLINE=false"
		set "GVM_ONLINE=true"
		echo Online mode re-enabled^!
	)
exit /b 0