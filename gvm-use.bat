@echo off

:gvmtool_use 
	set "CANDIDATE=%~1"
	call gvm-common gvmtool_check_candidate_present "%CANDIDATE%" || exit /b 1
	call gvm-common gvmtool_determine_version "%~2" || exit /b 1

	if exist "%GVM_HOME%\%CANDIDATE%\%VERSION%" (
		echo.
		echo Stop^! %CANDIDATE% %VERSION% is not installed.
		if not "%gvm_auto_answer%"=="true" (
			set /p INSTALL=Do you want to install it now? ^(Y/n^)^:  
		fi
		set "install_answer="
		if "%INSTALL%"=="" set "install_answer=true"
		if "%INSTALL%"=="y" set "install_answer=true"
		if "%INSTALL%"=="Y" set "install_answer=true" 
		if "%install_answer%"=="true" (
			rem call gvmtool_install_candidate_version "${CANDIDATE}" "${VERSION}"
		) else (
			exit /b 1
		)
	)

	:: Just update the *_HOME and PATH for this shell.
	for %%i in ('echo %CANDIDATE% | tr "[:lower:]" "[:upper:]"') do set "UPPER_CANDIDATE=%%i"
	call setx "%UPPER_CANDIDATE%_HOME" "%GVM_HOME%\%CANDIDATE%\%VERSION%" /M

	:: Replace the current path for the candidate with the selected version.
	if [[ "${solaris}" == true ]]; then
		export PATH=$(echo $PATH | gsed -r "s!${GVM_DIR}/${CANDIDATE}/([^/]+)!${GVM_DIR}/${CANDIDATE}/${VERSION}!g")

	elif [[ "${darwin}" == true ]]; then
		export PATH=$(echo $PATH | sed -E "s!${GVM_DIR}/${CANDIDATE}/([^/]+)!${GVM_DIR}/${CANDIDATE}/${VERSION}!g")

	else
		export PATH=$(echo $PATH | sed -r "s!${GVM_DIR}/${CANDIDATE}/([^/]+)!${GVM_DIR}/${CANDIDATE}/${VERSION}!g")
	fi

	echo ""
	echo Using "${CANDIDATE}" version "${VERSION} in this shell."
exit /b 0