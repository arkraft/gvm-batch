@echo off

:gvmtool_help
	echo.
	echo Usage: gvm ^<command^> ^<candidate^> [version]
	echo        gvm offline ^<enable^|disable^>
	echo.
	echo    commands:
	echo        install   or i    ^<candidate^> [version]
	echo        uninstall or rm   ^<candidate^> ^<version^>
	echo        list      or ls   ^<candidate^>
	echo        use       or u    ^<candidate^> [version]
	echo        default   or d    ^<candidate^> [version]
	echo        current   or c    [candidate]
	echo        version   or v
	echo        broadcast or b
	echo        help      or h
	echo        offline           ^<enable^|disable^>
	echo        selfupdate        [force]
	echo        flush             ^<candidates^|broadcast^|archives^|temp^>
	echo.
	echo    candidate  :  %GVM_CANDIDATES_CSV%
	echo. 		 
	echo    version    :  where optional, defaults to latest stable if not provided
	echo.
	echo eg: gvm install groovy
	echo.
exit /b 0