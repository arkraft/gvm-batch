@echo off 

:gvmtool_current
	if not "%~1"=="" (
		set "CANDIDATE=%~1"
		call gvm-common gvmtool_determine_current_version "!CANDIDATE!"
		if not "!CURRENT!"=="" (
			echo.
			echo Using !CANDIDATE! version !CURRENT!
		) else (
			echo.
			echo Not using any version of !CANDIDATE!
		)
	) else (
		set /A INSTALLED_COUNT=0
		for %%A in (%GVM_CANDIDATES_CSV%) do (
			:: Eliminate empty entries due to incompatibility
			if not "%%A"=="" (
				call gvm-common gvmtool_determine_current_version "%%A"
				if not "!CURRENT!"=="" (
					if !INSTALLED_COUNT! equ 0 (
						echo.
						echo Using^:
					)
					echo %%A	: !CURRENT!
					set /A INSTALLED_COUNT=!INSTALLED_COUNT!+1
					set "CURRENT="
				)
			)
		)
		if "%FIRST_ROUND%"=="true"  (
			echo.
			echo No candidates are in use
		)
	)
exit /b 0