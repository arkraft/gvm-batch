@echo off 

setlocal enabledelayedexpansion

set "COMMAND=%1"
set "QUALIFIER=%2"

if "%COMMAND%"=="l" (
    set "COMMAND=list"
) else if "%COMMAND%"=="ls" (
    set "COMMAND=list"
) else if "%COMMAND%"=="h" (
    set "COMMAND=help"
) else if "%COMMAND%"=="v" (
    set "COMMAND=version"
) else if "%COMMAND%"=="u" (
    set "COMMAND=use"
) else if "%COMMAND%"=="i" (
    set "COMMAND=install"
) else if "%COMMAND%"=="rm" (
    set "COMMAND=uninstall"
) else if "%COMMAND%"=="c" (
    set "COMMAND=current"
) else if "%COMMAND%"=="d" (
    set "COMMAND=default"
) else if "%COMMAND%"=="b" (
    set "COMMAND=broadcast"
)

:: Various variables
call "%GVM_HOME%\src\gvm-common.bat" gvmtool_default_environment_variables

:: create home if it does not exist
mkdir "%GVM_HOME%" > nul 2>&1

set "offline_command=false"
if "%COMMAND%"=="offline" (
    if "%QUALIFIER%"=="enable" (
        set "offline_command=true"
    )
)
if "%GVM_FORCE_OFFLINE%"=="true" set "offline_command=true"

set "BROADCAST_LIVE="
if not "%offline_command%"=="true" (
    set "BROADCAST_LIVE="
    set "var="
    set NLM=^


    for /f "tokens=* delims=" %%a in ('curl -s "%GVM_BROADCAST_SERVICE%/broadcast/latest"') do set "var=!var!!NLM! %%a"
    ::set var=!var:~1!
    set BROADCAST_LIVE=!var:~1!
    set "var="
    call "%GVM_HOME%\src\gvm-common.bat" gvm_check_offline
    if "%GVM_FORCE_OFFLINE%"=="true" set "BROADCAST_LIVE="
)

if "!BROADCAST_LIVE!"=="" (
    if "%GVM_ONLINE%"=="true" (
        if not "%COMMAND%"=="offline" (
            echo !OFFLINE_BROADCAST!
        )
    )   
)

if not "!BROADCAST_LIVE!"=="" (
    if "%GVM_ONLINE%"=="false" (
        echo !ONLINE_BROADCAST!
    )
)

if "!BROADCAST_LIVE!"=="" (
    set "GVM_ONLINE=false"
    set "GVM_AVAILABLE=false"
) else (
    set "GVM_ONLINE=true"
)

call "%GVM_HOME%\src\gvm-common.bat" gvmtool_update_broadcast "%COMMAND%"

:: Load the gvm config if it exists.
if exist "%GVM_HOME%\etc\config" (
    for /f "tokens=1* delims==" %%A IN (%GVM_HOME%\etc\config) DO (
        if "%%A"=="gvm_auto_answer" set "gvm_auto_answer=%%B"
        if "%%A"=="gvm_suggestive_selfupdate" set "gvm_suggestive_selfupdate=%%B"
        if "%%A"=="gvm_auto_selfupdate" set "gvm_auto_selfupdate=%%B"
    )
)

:: no command provided
if "%COMMAND%"=="" (
    call "%GVM_HOME%\src\gvm-help"
    exit /b 1
)


:: Check if it is a valid command
set "CMD_FOUND="
set "CMD_TARGET=%GVM_HOME%\src\gvm-%COMMAND%.bat"
if exist "%CMD_TARGET%" (
    set "CMD_FOUND=%CMD_TARGET%"
)

:: Check if it is a sourced function
set "CMD_TARGET=%GVM_HOME%\ext\gvm-%COMMAND%.bat"
if exist "%CMD_TARGET%" (
    set "CMD_FOUND=%CMD_TARGET%"
)

:: couldn't find the command
if "%CMD_FOUND%"=="" (
    echo Invalid command: %COMMAND%
    call gvm-help
)

:: Check whether the candidate exists
set "GVM_VALID_CANDIDATE="
for %%A in (%GVM_CANDIDATES_CSV%) do (
    if "%%A"=="%QUALIFIER%" (
        set "GVM_VALID_CANDIDATE=%QUALIFIER%"
    )
)

if not "%QUALIFIER%"=="" (
    if not "%COMMAND%"=="offline" (
        if not "%COMMAND%"=="flush" (
            if not "%COMMAND%"=="selfupdate" (
                if "%GVM_VALID_CANDIDATE%"=="" (
                    echo.
                    echo Stop^^! %QUALIFIER% is not a valid candidate.
                    exit /b 1
                )
            )
        )
    )
)
set "GVM_VALID_CANDIDATE="

if "%COMMAND%"=="offline" (
    if "%QUALIFIER%"=="" (
        echo.
        echo Stop^^! Specify a valid offline mode.
    ) else (
        set "offline_mode="
        if "%QUALIFIER%"=="enable" set "offline_mode=true"
        if "%QUALIFIER%"=="disable" set "offline_mode=true"
        if "!offline_mode!"=="" (
            echo.
            echo Stop^^! %QUALIFIER% is not a valid offline mode.
        )
    )
)

:: Execute the requested command
if not "%CMD_FOUND%"=="" (
    :: It's available as a batch subroutine
    call gvm-%COMMAND% "%QUALIFIER%" "%3" "%4"
)

:: Attempt upgrade after all is done
::if not "%$COMMAND%"=="selfupdate" (
::    call gvm-selfupdate gvmtool_auto_update "%GVM_REMOTE_VERSION%" "%GVM_VERSION%"
::)
endlocal && set "Path=%Path%" && set "GVM_FORCE_OFFLINE=%GVM_FORCE_OFFLINE%" && set "GVM_ONLINE=%GVM_ONLINE%"