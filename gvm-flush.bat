@echo off

goto:%1

:gvmtool_flush
	set "QUALIFIER=%~2"

	if "%QUALIFIER%"=="candidates" (
		if exist "%GVM_HOME%\var\candidates" (
			rm -f "%GVM_HOME%\var\candidates"
			echo.
			echo Candidates have been flushed.
			echo.
		) else (
			echo.
			echo No candidate list found so not flushed.
			echo.
		)
	) else if "%QUALIFIER%"=="broadcast" (
		if exist "%GVM_HOME%\var\broadcast" (
			rm -f "%GVM_HOME%\var\broadcast"
			echo.
			echo Broadcast has been flushed.
			echo.
		) else (
			echo.
			echo No prior broadcast found so not flushed.
			echo.
		)
	) else if "%QUALIFIER%"=="version" (
		if exist "%GVM_HOME%\var\version" (
			rm -f "%GVM_HOME%\var\version"
			echo.
			echo Version Token has been flushed.
			echo.
		) else (
			echo.
			echo No prior Remote Version found so not flushed.
			echo.
		)
	) else if "%QUALIFIER%"=="archives" (
		call :gvmtool_cleanup_folder "archives"
	) else if "%QUALIFIER%"=="temp" (
		call :gvmtool_cleanup_folder "tmp"
	) else if "%QUALIFIER%"=="tmp" (
		call :gvmtool_cleanup_folder "tmp"
	) else (
		echo.
		echo Stop^^! Please specify what you want to flush.
		echo.
	)
exit /b 0

:gvmtool_cleanup_folder
	::set "GVM_CLEANUP_DIR=%GVM_HOME%\%~1"
	::set "GVM_CLEANUP_DU=$(du -sh "$GVM_CLEANUP_DIR")
	::GVM_CLEANUP_COUNT=$(ls -1 "$GVM_CLEANUP_DIR" | wc -l)

	rm -rf "%GVM_HOME%\%~1"
	mkdir "%GVM_HOME%\%~1"

	::echo "${GVM_CLEANUP_COUNT} archive(s) flushed, freeing ${GVM_CLEANUP_DU}."
exit /b 0

