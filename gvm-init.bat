@echo off

setlocal enabledelayedexpansion

set "GVM_VERSION=0.1"

if "%GVM_SERVICE%"=="" (
    set "GVM_SERVICE=http://api.gvmtool.net"
)

if "%GVM_BROADCAST_SERVICE%"=="" (
    set "GVM_BROADCAST_SERVICE=http://cast.gvm.io"
)

if "%GVM_DIR%"=="" (
	set "GVM_DIR=%userProfile%\.gvm"
)

set LF=^



set "OFFLINE_BROADCAST===== BROADCAST =============================================!LF!!LF!OFFLINE MODE ENABLED! Some functionality is now disabled.!LF!!LF!============================================================"

set "ONLINE_BROADCAST===== BROADCAST =============================================!LF!!LF!ONLINE MODE RE-ENABLED! All functionality now restored.!LF!!LF!============================================================"

set "OFFLINE_MESSAGE=This command is not available in offline mode."

:: fabricate list of candidates
if exist "%GVM_HOME%\var\candidates" (
	for /f %%i in ('type "%GVM_HOME%\var\candidates"') do set "GVM_CANDIDATES_CSV=%%i"
) else (
    for /f %%i in ('curl -s "%GVM_SERVICE%/candidates"') do set "GVM_CANDIDATES_CSV=%%i"
	echo "%GVM_CANDIDATES_CSV%" > "%GVM_HOME\var\candidates"
)

:: initialise once only
if "%GVM_INIT%"=="true" (
    call :gvm_set_candidates
	call :gvm_source_modules
	exit /b 0
)

:: Build _HOME environment variables and prefix them all to PATH
for %%A in (%GVM_CANDIDATES_CSV%) do (
    :: Eliminate empty entries due to incompatibility
    if not "%%A"=="" (
        set "CANDIDATE_NAME=%%A"
        for /f %%i in ('echo !CANDIDATE_NAME! ^| tr "[:lower:]" "[:upper:]"') do set "CANDIDATE_HOME_VAR=%%i_HOME"
        set "CANDIDATE_DIR=%GVM_HOME%\!CANDIDATE_NAME!\current"
        setx !CANDIDATE_HOME_VAR! "!CANDIDATE_DIR!" > nul
        call pathman /au "!CANDIDATE_DIR!\bin"
        :: add to local environment
        set "Path=%Path%;!CANDIDATE_DIR!\bin"
        set "CANDIDATE_NAME="
        set "CANDIDATE_DIR="
        set "CANDIDATE_HOME_VAR="
    )
)

:: Load the gvm config if it exists.
if exist "%GVM_HOME%\etc\config" (
    for /f "tokens=1* delims==" %%A IN (%GVM_HOME%\etc\config) DO (
        if "%%A"=="gvm_auto_answer" set "gvm_auto_answer=%%B"
        if "%%A"=="gvm_suggestive_selfupdate" set "gvm_suggestive_selfupdate=%%B"
        if "%%A"=="gvm_auto_selfupdate" set "gvm_auto_selfupdate=%%B"
    )
)

:: Drop upgrade delay token if it does'nt exist
if not exist "%GVM_HOME%\var\delay_upgrade" (
	call touch "%GVM_HOME%\var\delay_upgrade"
)
echo hier
:: determine if up to date
set "GVM_VERSION_TOKEN=%GVM_HOME%\var\version"
if exist "%GVM_VERSION_TOKEN%" (
    for /f %%i in ('forfiles -p "%GVM_HOME%\var" -s -m "version" -d +1 -c "cmd /c echo @path" 2^>nul') do set "GVM_VERSION_TOKEN_DATE=%%i"
    if "%GVM_VERSION_TOKEN_DATE%"=="" (
        for /f %%i in ('type "%GVM_VERSION_TOKEN%"') do set "GVM_REMOTE_VERSION=%%i"
        goto:endIf
    ) else (
        goto:elseCase
    )
)
:elseCase
for /f %%i in ('curl -s "%GVM_SERVICE%/app/version" --connect-timeout 1 --max-time 1') do set "GVM_REMOTE_VERSION=%%i"
call :gvm_check_offline
set "orcase=false"
if "%GVM_REMOTE_VERSION%"=="" set "orcase=true"
if "%GVM_FORCE_OFFLINE%"=="true" set "orcase=true"
if "%orcase%"=="true" (
    set "GVM_REMOTE_VERSION=%GVM_VERSION%"
) else (
    echo %GVM_REMOTE_VERSION% > "%GVM_VERSION_TOKEN%"
)
set "orcase="
goto:endIf
setx GVM_INIT true > nul &2>1

exit /b 0

:gvm_check_offline 
    for /f %%a in ('curl -s "%GVM_SERVICE%/alive"') do set "SERVER_STAT=%%a"
    if not "%SERVER_STAT%"=="OK" (
        echo GVM can't reach the internet so going offline. Re-enable online with:
        echo.
        echo   ^>gvm offline disable
        echo. 
        set "GVM_FORCE_OFFLINE=true"
    )
    set "SERVER_STAT="
exit /b 0

endlocal && set "Path=%Path%" && set "OFFLINE_BROADCAST=%OFFLINE_BROADCAST%" && set "ONLINE_BROADCAST=%ONLINE_BROADCAST%" && set "GVM_CANDIDATES_CSV=%GVM_CANDIDATES_CSV%" && set "GVM_OFFLINE_MESSAGE=%GVM_OFFLINE_MESSAGE"