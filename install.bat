@echo off
SETLOCAL EnableDelayedExpansion

set "GVM_SERVICE=http://api.gvmtool.net"
set "GVM_BROADCAST_SERVICE=http://cast.gvm.io"
set "GVM_HOME=%userProfile%\.gvm"
set "GVM_VERSION=0.1"

set "gvm_bin_folder=%GVM_HOME%\bin"
set "gvm_src_folder=%GVM_HOME%\src"
set "gvm_tmp_folder=%GVM_HOME%\tmp"
set "gvm_stage_folder=%GVM_HOME%\stage"
set "gvm_zip_file=%GVM_HOME%\res-%GVM_VERSION%.zip"
set "gvm_ext_folder=%GVM_HOME%\ext"
set "gvm_etc_folder=%GVM_HOME%\etc"
set "gvm_var_folder=%GVM_HOME%\var"
set "gvm_config_file=%GVM_HOME%\config"

echo.
echo Thanks for using
echo.
echo _____/\\\\\\\\\\\\__/\\\________/\\\__/\\\\____________/\\\\_
echo  ___/\\\//////////__\/\\\_______\/\\\_\/\\\\\\________/\\\\\\_
echo   __/\\\_____________\//\\\______/\\\__\/\\\//\\\____/\\\//\\\_
echo    _\/\\\____/\\\\\\\__\//\\\____/\\\___\/\\\\///\\\/\\\/_\/\\\_
echo     _\/\\\___\/////\\\___\//\\\__/\\\____\/\\\__\///\\\/___\/\\\_
echo      _\/\\\_______\/\\\____\//\\\/\\\_____\/\\\____\///_____\/\\\_
echo       _\/\\\_______\/\\\_____\//\\\\\______\/\\\_____________\/\\\_
echo        _\//\\\\\\\\\\\\/_______\//\\\_______\/\\\_____________\/\\\_
echo         __\////////////__________\///________\///______________\///__
echo.
echo                                        Will now attempt installing...
echo.

echo|set /p=Looking for a previous installation of GVM...

if exist "%GVM_HOME%" (
    call :c 0e "installation found"
    echo.
    echo ======================================================================================================
    echo  You already have GVM installed.
    echo  GVM was found at:
    echo.
    echo     %GVM_HOME%
    echo.
    echo  Please consider running the following if you need to upgrade.
    echo.
    echo ======================================================================================================
    echo.
    exit /B 0
) else (
    call :c 0A "not installed"
    echo.
)


for /f "delims=" %%G in ('where unzip') do set "unziplocation=%%G"
echo|set /p=Looking for unzip...
if "%unziplocation%"=="" (
	call :c 0c "Not found."
	echo.
	echo ======================================================================================================
	echo  Please install unzip on your system.
	echo.
	echo  Restart after installing unzip.
	echo ======================================================================================================
	echo.
	exit /B 0
) else (
    call :c 0A "found!"
    echo.
)

for /f "delims=" %%G in ('where curl') do set "curllocation=%%G"
echo|set /p=Looking for curl...
if "%curllocation%"=="" (
	call :c 0c "Not found."
    echo.
	echo ======================================================================================================
	echo Please install curl on your system.
	echo.
	echo GVM uses curl for crucial interactions with it's backend server.
	echo.
	echo Restart after installing curl.
	echo ======================================================================================================
	echo.
	exit /B 0
) else (
    call :c 0A "found!"
    echo.
)

for /f "delims=" %%G in ('where touch') do set "touchlocation=%%G"
echo|set /p=Looking for touch...
if "%touchlocation%"=="" (
    call :c 0c "Not found."
    echo.
    echo ======================================================================================================
    echo Please install touch on your system.
    echo.
    echo Restart after installing touch.
    echo ======================================================================================================
    echo.
    exit /B 0
) else (
    call :c 0A "found!"
    echo.
)

for /f "delims=" %%G in ('where junction') do set "junctionlocation=%%G"
echo|set /p=Looking for junction...
if "%junctionlocation%"=="" (
    call :c 0c "Not found."
    echo.
    echo ======================================================================================================
    echo Please install junction on your system.
    echo.
    echo GVM uses juntion to create and read symbolic links.
    echo.
    echo Restart after installing junction.
    echo ======================================================================================================
    echo.
    exit /B 0
) else (
    call :c 0A "found!"
    echo.
)

for /f "delims=" %%G in ('where tr') do set "trlocation=%%G"
echo|set /p=Looking for tr...
if "%trlocation%"=="" (
    call :c 0c "Not found."
    echo.
    echo ======================================================================================================
    echo Please install tr on your system.
    echo.
    echo GVM uses tr for essential string conversion
    echo.
    echo Restart after installing tr.
    echo ======================================================================================================
    echo.
    exit /B 0
) else (
    call :c 0A "found!"
    echo.
)

for /f "delims=" %%G in ('where pathman') do set "pathmanlocation=%%G"
echo|set /p=Looking for pathman...
if "%pathmanlocation%"=="" (
    call :c 0c "Not found."
    echo.
    echo ======================================================================================================
    echo Please install pathman on your system.
    echo.
    echo GVM uses pathman for setting and removing path variables
    echo.
    echo Restart after installing pathman.
    echo ======================================================================================================
    echo.
    exit /B 0
) else (
    call :c 0A "found!"
    echo.
)

echo|set /p=Create distribution directories...

mkdir "%gvm_bin_folder%"
mkdir "%gvm_src_folder%"
mkdir "%gvm_tmp_folder%"
mkdir "%gvm_stage_folder%"
mkdir "%gvm_ext_folder%"
mkdir "%gvm_etc_folder%"
mkdir "%gvm_var_folder%"

call :c 0A "done!"
echo.

echo|set /p=Create candidate directories...

for /f %%i in ('curl -s "%GVM_SERVICE%/candidates"') do set "GVM_CANDIDATES_CSV=%%i"

@echo %GVM_CANDIDATES_CSV% > "%gvm_var_folder%\candidates"
@echo %GVM_VERSION% > "%gvm_var_folder%\version"

for %%a in (%GVM_CANDIDATES_CSV%) do (
    if not "%%a"=="" (
        setlocal
        mkdir "%GVM_HOME%\%%a"
        endlocal
    )
)

call :c 0A "done!"

echo|set /p=Prime the config file...
echo gvm_auto_answer=false > "%gvm_config_file%"
echo gvm_auto_selfupdate=false >> "%gvm_config_file%"
call :c A0 "done!"

echo Download script archive...
echo URL: https://github.com/arkraft/gvm-batch/releases/download/gvm-batch-%GVM_VERSION%/res-%GVM_VERSION%.zip
curl -L -s "https://github.com/arkraft/gvm-batch/releases/download/gvm-batch-%GVM_VERSION%/res-%GVM_VERSION%.zip" > "%gvm_zip_file%"

unzip -qo "%gvm_zip_file%" -d "%gvm_stage_folder%"

echo Install scripts...
move "%gvm_stage_folder%\gvm-init.bat" "%gvm_bin_folder%"
move "%gvm_stage_folder%\gvm.bat" "%gvm_bin_folder%"
pathman /au "%gvm_bin_folder%\"
set "Path=%Path%;%gvm_bin_folder%"
move "%gvm_stage_folder%"\gvm-* "%gvm_src_folder%"

echo.
echo All done!
echo.
echo.

echo Please open a new terminal, or run the following in the existing one^:
echo.
echo     call "%GVM_DIR%\bin\gvm-init.bat"
echo.
echo Then issue the following command^:
echo.
echo     gvm help
echo.
echo Enjoy!!!
echo.
exit /b 0

::::::::::::::::::::::::::: colors :::::::::::::::::::::::::::

:c
    setlocal enableDelayedExpansion
    :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    :colorPrint Color  Str  [/n]
    setlocal
    set "s=%~2"
    call :colorPrintVar %1 s %3
    exit /b

    :colorPrintVar  Color  StrVar  [/n]
    if not defined DEL call :initColorPrint
    setlocal enableDelayedExpansion
    pushd .
    ':
    cd \
    set "s=!%~2!"
    :: The single blank line within the following IN() clause is critical - DO NOT REMOVE
    for %%n in (^"^

    ^") do (
      set "s=!s:\=%%~n\%%~n!"
      set "s=!s:/=%%~n/%%~n!"
      set "s=!s::=%%~n:%%~n!"
    )
    for /f delims^=^ eol^= %%s in ("!s!") do (
      if "!" equ "" setlocal disableDelayedExpansion
      if %%s==\ (
        findstr /a:%~1 "." "\'" nul
        <nul set /p "=%DEL%%DEL%%DEL%"
      ) else if %%s==/ (
        findstr /a:%~1 "." "/.\'" nul
        <nul set /p "=%DEL%%DEL%%DEL%%DEL%%DEL%"
      ) else (
        >colorPrint.txt (echo %%s\..\')
        findstr /a:%~1 /f:colorPrint.txt "."
        <nul set /p "=%DEL%%DEL%%DEL%%DEL%%DEL%%DEL%%DEL%"
      )
    )
    if /i "%~3"=="/n" echo(
    popd
    exit /b


    :initColorPrint
    for /f %%A in ('"prompt $H&for %%B in (1) do rem"') do set "DEL=%%A %%A"
    <nul >"%temp%\'" set /p "=."
    subst ': "%temp%" >nul
    exit /b


    :cleanupColorPrint
    2>nul del "%temp%\'"
    2>nul del "%temp%\colorPrint.txt"
    >nul subst ': /d
    exit /b 0
endlocal

