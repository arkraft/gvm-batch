@echo off

goto:%1

:::
::Checks if a candidate was given, returns exit code 1 if not
:::
:gvmtool_check_candidate_present
	if "%~2"=="" (
		echo.
		echo No candidate provided.
		call gvm-help
		exit /b 1
	)
exit /b 0

:gvmtool_check_version_present
	if "%~2"=="" (
		echo No candidate version provided
		call gvm-help
		exit /b 1
	)
exit /b 0

:::
::Sets default variables 
:::
:gvmtool_default_environment_variables
	if "%GVM_FORCE_OFFLINE%"=="" (
		set "GVM_FORCE_OFFLINE=false"
	)
	if "%GVM_ONLINE%"=="" (
		set "GVM_ONLINE=true"
	)
	set "condition=false"
	if "%GVM_ONLINE%"=="false" set "condition=true"
	if "%GVM_FORCE_OFFLINE"=="true" set "condition=true"
	if "%condition%"=="true" (
		set "GVM_AVAILABLE=false"
	) else (
	  	set "GVM_AVAILABLE=true"
	)
exit /b 0

:gvm_check_offline 
	for /f %%a in ('curl -s "%GVM_SERVICE%/alive"') do set "SERVER_STAT=%%a"
	if not "%SERVER_STAT%"=="OK" (
		echo GVM can't reach the internet so going offline. Re-enable online with:
		echo.
		echo   ^>gvm offline disable
		echo. 
		set "GVM_FORCE_OFFLINE=true"
	)
exit /b 0


:::
::Determines if the given candidate version is a valid version
:::
:gvmtool_determine_version
	if "!GVM_AVAILABLE!"=="false" (
		if not "%~2"=="" (
			if exist "%GVM_HOME%\%CANDIDATE%\%~2" (
				set "VERSION=%~2"
				goto :endofcheck
			)
		)
	) 
	if "!GVM_AVAILABLE!"=="false" (
		if "%~2"=="" (
			for %%i in ("%GVM_HOME%\%CANDIDATE%\current") do set attribs=%%~ai
			if "%attribs:~-1%" == "l" (
				for /f "tokens=1,2,3 delims=:" %%i in ('junction -q "%GVM_HOME%\%CANDIDATE%\current"') do (
					echo.%%i | findstr /C:"Substitute">nul && (
				    	set target=%%j:%%k
				    	for /F "tokens=*" %%s in ("%target%") do set target=%%s
				    	set "VERSION=!target!"
						goto :endofcheck
					)
				)
			)
		)
	)

	if "!GVM_AVAILABLE!"=="false" (
		if not "%~2"=="" (
			echo Stop! %CANDIDATE% %1 is not available in offline mode.
			exit /b 1
		)
	)

	if "!GVM_AVAILABLE!"=="false" (
		if "%~2"=="" (
			echo ==== BROADCAST =============================================
            echo.
            echo OFFLINE MODE ENABLED! Some functionality is now disabled.
            echo.
            echo ============================================================
            exit /b 1
		)
	)

	if "!GVM_AVAILABLE!"=="true" (
		if "%~2"=="" (
			set "VERSION_VALID=valid"
			for /f %%a in ('curl -s "%GVM_SERVICE%/candidates/%CANDIDATE%/default"') do set "VERSION=%%a"
			goto :endofcheck
		)
	)
	for /f %%a in ('curl -s "%GVM_SERVICE%/candidates/%CANDIDATE%/%~2"') do set "VERSION_VALID=%%a"
	set "valid=false"
	if "%VERSION_VALID%"=="valid" set "valid=true"
	if "%VERSION_VALID%"=="invalid" (
		if not "%3"=="" (
			set "valid=true"
			goto :endofcheck
		)
	)
	if "%valid%"=="true" (
		set "VERSION=%~2"
		goto :endofcheck
	)
	if "%VERSION_VALID%"=="invalid" (
		for %%i in ("%GVM_HOME%\%CANDIDATE%\%~2") do set attribs=%%~ai
		if "%attribs:~-1%" == "l" (
			set "VERSION=%~2"
			goto :endofcheck
		)
	)

	if "%VERSION_VALID"=="invalid" (
		if exist "%GVM_HOME%\%CANDIDATE%\%1" (
			set "VERSION=%~2"
			goto :endofcheck
		)
	)
	echo.
	echo Stop! %~2 is not a valid %CANDIDATE% version.
	exit /b 1
	:endofcheck
exit /b 0

:gvmtool_link_candidate_version
	set "CANDIDATE=%~2"
	set "VERSION=%~3"

	:: Change the 'current' symlink for the candidate, hence affecting all shells.
	if exist "%GVM_HOME%\%CANDIDATE%\current" (
		call junction -d "%GVM_HOME%\%CANDIDATE%\current" > nul
	)
	call junction "%GVM_HOME%\%CANDIDATE%\current" "%GVM_HOME%\%CANDIDATE%\%VERSION%" > nul
exit /b 0

:gvmtool_determine_current_version
	set "CANDIDATE=%~2"
	for /f "delims=" %%f in ('where %CANDIDATE% 2^> nul') do (
		@echo.%%f | findstr /C:"%CANDIDATE%.bat">nul || (
			set "name=%%f"
			set "name=!name:\bin\%CANDIDATE%=!"
			for %%F in ("!name!") do set CURRENT=%%~nxF
		)
	)

	if "!CURRENT!"=="current" (
		set "CURRENT="
	)

	if "!CURRENT!"=="" (
		for /f "tokens=1,2,3 delims=:" %%i in ('junction -q "%GVM_HOME%\%CANDIDATE%\current"') do (
			echo.%%i | findstr /C:"Substitute">nul && (
		    	set target=%%j:%%k
		    	for %%F in ("!target!") do set CURRENT=%%~nxF
				goto :endofcheck
			)
		)
	)
	
exit /b 0

:gvmtool_update_broadcast
	set "COMMAND=%~2"
	set "BROADCAST_FILE=%GVM_HOME%\var\broadcast"
	set LF=^


	set "BROADCAST_HITS="
	if exist "%BROADCAST_FILE%" (
		for /f "delims=" %%G in ('type "%BROADCAST_FILE%"') do set "BROADCAST_HITS=!BROADCAST_HITS!!LF!%%G"
	)

	if "!GVM_AVAILABLE!"=="true" (
		if "!BROADCAST_HITS!"=="!BROADCAST_LIVE!" (
			if not "%COMMAND%"=="broadcast" (
				if not "%COMMAND%"=="selfupdate" (
					if not "%COMMAND%"=="flush" (
						if not exist "%GVM_HOME%\var" mkdir "%GVM_HOME%\var"
						@echo !BROADCAST_LIVE! > "%BROADCAST_FILE%"
						echo !BROADCAST_LIVE!
					)
				)
			)
		)
	)
	set "BROADCAST_FILE="
exit /b 0