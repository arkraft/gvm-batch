@echo off
setlocal enabledelayedexpansion

set "CSV="
goto :gvmtool_list

:gvmtool_build_version_csv
set "CANDIDATE=%~1"
for /f "delims=|" %%f in ('dir /b "%GVM_HOME%\%CANDIDATE%"') do (
	if not "%%f"=="current" (
		set "CSV=%%f,!CSV!"
	)
)
exit /b 0

:gvmtool_offline_list
	setlocal
	echo.
	echo ------------------------------------------------------------
	echo Offline Mode: only showing installed %CANDIDATE% versions
	echo ------------------------------------------------------------
	echo.                                                             
	for %%c in (%CSV%) do (
		if not "%%c"=="" (
			if "%%c" == "%CURRENT%" (
				echo  ^> %%c
			) else (
				echo  ^* %%c
			)
		)
	)

	if "!CSV!"=="" (
		echo    None installed!
	)

	echo.
	echo ------------------------------------------------------------
	echo ^* - installed                                               
	echo ^> - currently in use                                        
	echo ------------------------------------------------------------
	endlocal
exit /b 0

:gvmtool_list
	set "CANDIDATE=%~1"
	call gvm-common gvmtool_check_candidate_present "%CANDIDATE%" || exit /b 1
	call :gvmtool_build_version_csv "%CANDIDATE%" || exit /b 1
	call gvm-common gvmtool_determine_current_version "%CANDIDATE%"

	if "!GVM_AVAILABLE!"=="false" (
		call :gvmtool_offline_list
	) else (
		echo.
		for /f "tokens=* delims=" %%i in ('curl -s "%GVM_SERVICE%/candidates/%CANDIDATE%/list?platform=cygwin&current=!CURRENT!&installed=%CSV%"') do echo. %%i
	)
exit /b 0