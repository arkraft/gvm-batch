@echo off

:gvmtool_default
	set "CANDIDATE=%~1"
	call gvm-common gvmtool_check_candidate_present "%CANDIDATE%" || exit /b 1
	call gvm-common gvmtool_determine_version "%~2" || exit /b 1

	if not exist "%GVM_HOME%\%CANDIDATE%\%VERSION%" (
		echo.
		echo Stop^^! %CANDIDATE% %VERSION% is not installed.
		echo.
		exit /b 1
	)

	call gvm-common gvmtool_link_candidate_version "%CANDIDATE%" "%VERSION%"

	echo.
	echo Default %CANDIDATE% version set to %VERSION%.
	echo.
exit /b 0