@echo off

:gvmtool_uninstall 
	set "CANDIDATE=%~1"
	set "VERSION=%~2"
	call gvm-common gvmtool_check_candidate_present "%CANDIDATE%" || exit /b 1
	call gvm-common gvmtool_check_version_present "%VERSION%" || exit /b 1
	CURRENT=$(readlink "${GVM_DIR}/${CANDIDATE}/current" | sed "s_${GVM_DIR}/${CANDIDATE}/__g")
	for %%i in ("%~1") do set attribs=%%~ai
	if "%attribs:~-1%" == "l" (
		for /f "tokens=1,2,3 delims=:" %%i in ('junction -q "test1"') do (
			echo.%%i | findstr /C:"Substitute">nul && (
		    	set target=%%j:%%k
		    	for /F "tokens=*" %%s in ("%target%") do set target=%%s
		    	echo !target!
		    	if "!target!"=="%VERSION%" (
					echo.
					echo Unselecting %CANDIDATE% %VERSION%...
					call junction -d "%GVM_DIR%\%CANDIDATE%\current"
				)
			)
		)
	)
	echo.
	if exist "%GVM_HOME%\%CANDIDATE%\%VERSION%" (
		echo Uninstalling %CANDIDATE% %VERSION%...
		call rm -rf "%GVM_HOME%\%CANDIDATE%\%VERSION%"
	) else (
		echo %CANDIDATE% %VERSION% is not installed.
	)
}