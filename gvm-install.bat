@echo off

:gvmtool_install
	set "CANDIDATE=%~1"
	set "LOCAL_FOLDER=%~3"
	call gvm-common gvmtool_check_candidate_present "%CANDIDATE%" || exit /b 1
	call gvm-common gvmtool_determine_version "%~2" "%~3" || exit /b 1
	set "valid=false"
	if exist "%GVM_HOME%\%CANDIDATE%\%VERSION%" set "valid=true"
	for %%i in ("%GVM_HOME%\%CANDIDATE%\%VERSION%") do set attribs=%%~ai
	if "%attribs:~-1%"=="l" set "valid=true"
	if "%valid%"=="true" (
		echo.
		echo Stop^^! %CANDIDATE% !VERSION! is already installed
		exit /b 0
	)
	if "%VERSION_VALID%"=="valid" (
		call :gvmtool_install_candidate_version "%CANDIDATE%" "%VERSION%" || exit /b 1

		set "USE="
		if not "%gvm_auto_answer%"=="true" (
			set /p USE=Do you want %CANDIDATE% %VERSION% to be set as default? ^(Y/n^)^:  
		)

		:: check if no answer was provided or if it is y or Y
		set "answered=false"
		if "!USE!"=="" set "answered=true"
		if "!USE!"=="y" set "answered=true"
		if "!USE!"=="Y" set "answered=true"
		
		if "!answered!"=="true" (
			echo.
			echo Setting %CANDIDATE% %VERSION% as default.
			call gvm-common gvmtool_link_candidate_version "%CANDIDATE%" "%VERSION%"
		)

		:: unset variables
		set "USE="
		set "answered="
		echo.
		exit /b 0
	)
	if "%VERSION_VALID%"=="valid" (
	
	) else (
		if not "%LOCAL_FOLDER%"=="" (
			call :gvmtool_install_local_version "%CANDIDATE%" "%VERSION%" "%LOCAL_FOLDER%" || exit /b 1
			goto :endoffunction
		)
	)
    echo.
	echo Stop! %1 is not a valid %CANDIDATE% version.
	exit /b 1
	:endoffunction
exit /b 0

:gvmtool_download
	set "CANDIDATE=%~1"
	set "VERSION=%~2"
	if not exist "%GVM_HOME%\archives" mkdir "%GVM_HOME%\archives"
	if not exist "%GVM_HOME%\archives\%CANDIDATE%-%VERSION%.zip" (
		echo.
		echo Downloading: %CANDIDATE% %VERSION%
		echo.
		set "DOWNLOAD_URL=%GVM_SERVICE%/download/%CANDIDATE%/%VERSION%?platform=cygwin"
		set "ZIP_ARCHIVE=%GVM_HOME%\archives\%CANDIDATE%-%VERSION%.zip"
		call curl -L "!DOWNLOAD_URL!" > "!ZIP_ARCHIVE!"
		call :gvmtool_validate_zip "!ZIP_ARCHIVE!" || exit /b 1
	) else (
		echo.
		echo Found a previously downloaded %CANDIDATE% %VERSION% archive. Not downloading it again...
		call :gvmtool_validate_zip "%GVM_HOME%\archives\%CANDIDATE%-%VERSION%.zip" || exit /b 1
	)
	echo.
exit /b 0

:gvmtool_validate_zip
	set "ZIP_ARCHIVE=%~1"
	call unzip -q -t "%ZIP_ARCHIVE%" > nul
	for /f "delims=" %%f in ('unzip -q -t "%ZIP_ARCHIVE%" ^| findstr "No errors detected in compressed data"') do set "ZIP_OK=%%f"
	if "%ZIP_OK%"=="" (
		call rm -f "%ZIP_ARCHIVE%"
		echo.
		echo Stop! The archive was corrupt and has been removed! Please try installing again.
		exit /b 1
	)
exit /b 0

:gvmtool_install_local_version
	set "CANDIDATE=%~1"
	set "VERSION=%~2"
	set "LOCAL_FOLDER=%3"
	mkdir "%GVM_DIR%\%CANDIDATE%" > nul 2>&1

	echo Linking %CANDIDATE% %VERSION% to %LOCAL_FOLDER%
	call junction "%GVM_HOME%\%CANDIDATE%\%VERSION%" "%LOCAL_FOLDER%" > nul
	echo Done installing!
exit /b 0

:gvmtool_install_candidate_version
	set "CANDIDATE=%~1"
	set "VERSION=%~2"
	call :gvmtool_download "%CANDIDATE%" "%VERSION%" || exit /b 1
	echo Installing^: %CANDIDATE% %VERSION%

	if not exist "%GVM_HOME%\%CANDIDATE%" mkdir "%GVM_HOME%\%CANDIDATE%"

	unzip -oq "%GVM_HOME%\archives\%CANDIDATE%-%VERSION%.zip" -d "%GVM_HOME%\tmp" > nul
	mv "%GVM_HOME%\tmp\%CANDIDATE%-%VERSION%" "%GVM_HOME%\%CANDIDATE%\%VERSION%"
	echo Done installing!
	echo.
exit /b 0
