gvm-batch
=========

This is the batch version of the [gvm-cli](https://github.com/gvmtool/gvm-cli) repository. 

it uses the commands and the api of gvm cli and is nothing more than a bash batch conversion of gvm-cli, 
so most of the credits goes to [Marco Vermeulen](https://github.com/marcoVermeulen) who created the gvm tool.

### Requirenments

gvm-batch has some requirenments before you can work with it. 

- cURL for windows, it is used for downloading the archives and can be obtained [here](http://curl.haxx.se/download.html)
- 7zip for unzipping and testing the archives, [here](http://www.7-zip.de/)
- junction, for easily creating, reading and deleting junctions (symlinks) in windows, [here](http://technet.microsoft.com/de-de/sysinternals/bb896768.aspx)
- tr for windows, it is used for string manipulation and can be obtained [here](http://unxutils.sourceforge.net/)
- pathman, which is used for adding and removing paths to/from the path variable, [here](http://download.microsoft.com/download/win2000platform/pathman/1.00.0.1/nt5/en-us/pathman_setup.exe)

All these tools have to be installed and their paths must be added to the path variable so they can be executed within the cmd window.

### Installation


### Usage
